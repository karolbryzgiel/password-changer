package com.example.passwrdchanger.model;


import org.hibernate.validator.constraints.Length;

public class PasswordResetDto {

    @Length(min = 5, message = "Hasło musi mieć minimum 5 znaków")
    private String firstPassword;
    @Length(min = 5, message = "Hasło musi mieć minimum 5 znaków")
    private String confirmPassword;



    public String getFirstPassword() {
        return firstPassword;
    }

    public void setFirstPassword(String firstPassword) {
        this.firstPassword = firstPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}