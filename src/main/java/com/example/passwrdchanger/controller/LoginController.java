package com.example.passwrdchanger.controller;

import javax.naming.Binding;
import javax.validation.Valid;

import com.example.passwrdchanger.model.PasswordResetDto;
import com.example.passwrdchanger.model.User;
import com.example.passwrdchanger.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @RequestMapping(value = "/admin/home", method = RequestMethod.POST)
    public ModelAndView editPasswordRequest(@Valid PasswordResetDto passwordResetDto, BindingResult bindingResult) {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User newUser = userService.findUserByEmail(auth.getName());

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("admin/home");
        }
        else if ( passwordResetDto.getFirstPassword().equals(passwordResetDto.getConfirmPassword())) {
            newUser.setPassword(passwordResetDto.getFirstPassword());
            userService.saveUser(newUser);
            //modelAndView.addObject("successMessage", "Hasło zostało zmienione");
            return new ModelAndView("redirect:/logout");
        }
        else {
            bindingResult
                    .rejectValue("confirmPassword", "error.passwordResetDto",
                            "Oba hasła muszą być takie same.");

        }

        modelAndView.addObject("passwordResetDto", passwordResetDto);
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }

    @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        PasswordResetDto passwordResetDto = new PasswordResetDto();

        //modelAndView.addObject("userName", "Witaj " + user.getName() + " " + user.getLastName());
        //modelAndView.addObject("email", user.getEmail());
        modelAndView.addObject("user", user);
        modelAndView.addObject("passwordResetDto", passwordResetDto);
        modelAndView.setViewName("admin/home");
        return modelAndView;
    }


}