package com.example.passwrdchanger.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;


public final class NoOpPasswordEncoder implements PasswordEncoder {

    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return rawPassword.toString().equals(encodedPassword);
    }

}
