package com.example.passwrdchanger.service;

import java.util.Arrays;
import java.util.HashSet;

import com.example.passwrdchanger.controller.NoOpPasswordEncoder;
import com.example.passwrdchanger.model.Role;
import com.example.passwrdchanger.model.User;
import com.example.passwrdchanger.repository.RoleRepository;
import com.example.passwrdchanger.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    //@Autowired
    // private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

}