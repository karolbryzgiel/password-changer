package com.example.passwrdchanger.service;

import com.example.passwrdchanger.model.User;

public interface UserService {
    public User findUserByEmail(String email);
    public void saveUser(User user);
}